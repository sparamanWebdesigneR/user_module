package com.project.Transactions.accounting.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.Transactions.accounting.Entity.Transactions;


public interface TransactionsRepository extends JpaRepository <Transactions,Integer> {

}
