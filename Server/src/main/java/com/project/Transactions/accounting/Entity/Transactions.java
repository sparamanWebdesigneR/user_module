package com.project.Transactions.accounting.Entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name ="Entrytable")
public class Transactions {
	
	@Id
	@GeneratedValue
	  private int  id;
      private String type;
	  private String mode;
	  private String description;
	  private String datetime;
	  private String createdby;
	  private String updatedby;
	  private String whatforCredit;
	  private String whatforDebit;
	  private String createddate;
	  private String updateddate;
	  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public String getWhatforCredit() {
		return whatforCredit;
	}
	public void setWhatforCredit(String whatforCredit) {
		this.whatforCredit = whatforCredit;
	}
	public String getWhatforDebit() {
		return whatforDebit;
	}
	public void setWhatforDebit(String whatforDebit) {
		this.whatforDebit = whatforDebit;
	}
	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
	public String getUpdateddate() {
		return updateddate;
	}
	public void setUpdateddate(String updateddate) {
		this.updateddate = updateddate;
	}

}
