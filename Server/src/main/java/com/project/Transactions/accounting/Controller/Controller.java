package com.project.Transactions.accounting.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.project.Transactions.accounting.Entity.Transactions;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.project.Transactions.accounting.Service.TransactionsService;

@RestController
@CrossOrigin
public class Controller {
	
	@Autowired
	 private TransactionsService service;
	@PostMapping ("/transactionId")
	public Transactions addTransactions (@RequestBody Transactions transactions) {
		 return service.saveTransactions(transactions);
	}
	@PostMapping ("/transactionIds")
	public List<Transactions> addTransactions (@RequestBody List<Transactions> transactions){
	   return service.saveTansactions(transactions);
	}
	@GetMapping("/transaction")
	public List<Transactions> findAllTransactions(){
		return  service.getTransactions();
	}
	@GetMapping("/transactionById/Id}")
		public  Transactions  findTransactionsById (@PathVariable int Transactionsid) {
		   return service.getTransactionsById(Transactionsid);
	}
	@PutMapping("/UpdateId")
	public  Transactions  updateTransactions (@RequestBody Transactions transactions) {
		 return service.updateTransactions(transactions);
	}
	
   @DeleteMapping ("/deletetransactions/{Id}")  
	   public  String deleteTransaactions (@PathVariable int Transactionsid) {
   	 return service.deleteTransctions(Transactionsid);
   }

}
