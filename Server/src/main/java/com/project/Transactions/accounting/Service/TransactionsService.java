package com.project.Transactions.accounting.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import com.project.Transactions.accounting.Entity.Transactions;
import com.project.Transactions.accounting.Repository.TransactionsRepository;

@Service
public class TransactionsService {
	
	@Autowired
	   private TransactionsRepository repository;
    public Transactions saveTransactions (Transactions transactions) {
 	    return repository.save(transactions);
     }
    public List<Transactions> saveTansactions (List<Transactions> transactions){
 	    return repository.saveAll(transactions);
    }
    public Transactions getTransactionsById(int Transactionsid) {
 	   return repository.findById(Transactionsid).orElse(null);
    }
    public String deleteTransctions(int Transactionsid) {
 	   repository.deleteById(Transactionsid);
 	   return "Transactionsid remove !!"+Transactionsid;
    }
    public Transactions updateTransactions(Transactions transactions) {  
 	   Transactions existingTransactions=repository.findById(transactions.getId()).orElse(null);
 	   existingTransactions.setId(transactions.getId());
 	   existingTransactions.setType(transactions.getType());
 	   existingTransactions.setMode(transactions.getMode());
 	   existingTransactions.setDescription(transactions.getDescription());
 	   existingTransactions.setDatetime(transactions.getDatetime());
 	   existingTransactions.setCreatedby(transactions.getCreatedby());
 	   existingTransactions.setUpdatedby(transactions.getUpdatedby());
 	   existingTransactions.setWhatforCredit(transactions.getWhatforCredit());
 	   existingTransactions.setWhatforDebit(transactions.getWhatforDebit());
 	   existingTransactions.setCreateddate(transactions.getCreateddate());
 	   existingTransactions.setUpdateddate(transactions.getUpdateddate());
 	      return repository.save(existingTransactions);
    }
    

}
